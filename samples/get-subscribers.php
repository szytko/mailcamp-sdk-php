<?php
define('USE_AUTOLOADER', true);
require_once '../Mailcamp/autoload.php';

try {
    $api = new Mailcamp_Api('#', '26fa7004a93d4ddfda6bbf53a1f6128e20381e2d', 'http://mailcamp.eu/development/xml.php');

    $response = $api->isSubscriberOnList(array(
        'mailinglist' =>   11,
        'emailaddress' => 'slawomir.zytko@gmail.com'
    ));

    print_r($response);

    if ($response->isSuccess) echo 'Success';
    else throw new Exception($response->toArray['errormessage']);
} catch (Exception $ex) {
    echo $ex->getTraceAsString();
    echo $ex->getMessage();
}
?>