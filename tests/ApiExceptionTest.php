<?php
/**
 *
 * @author    Sławomir Żytko <slawomir.zytko@gmail.com>
 * @copyright (c) 2013, PitGroup (http://pitgroup.nl)
 */

if (!defined('USE_AUTOLOADER')) define('USE_AUTOLOADER', false);
require_once '../Mailcamp/autoload.php';

class ApiExceptionTest extends PHPUnit_Framework_TestCase {

    public function testExceptionInvalidUsername() {
        $this->setExpectedException('InvalidUsernameException');

        new Mailcamp_Api(null, null, null);
    }

    public function testExceptionInvalidUsertoken() {
        $this->setExpectedException('InvalidUsertokenException');

        new Mailcamp_Api('username', null, null);
    }

    public function testExceptionInvalidEndpoint() {
        $this->setExpectedException('InvalidEndpointException');

        new Mailcamp_Api('username', 'usertoken', null);
    }
}
