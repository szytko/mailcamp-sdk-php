<?php
/**
 *
 * @author    Sławomir Żytko <slawomir.zytko@gmail.com>
 * @copyright (c) 2013, PitGroup (http://pitgroup.nl)
 */
if (!defined('USE_AUTOLOADER')) define('USE_AUTOLOADER', false);
require_once '../Mailcamp/autoload.php';

define('USERNAME', '#YOUR_USERNAME#');
define('USERTOKEN', '#YOUR_USERTOKEN#');
define('ENDPOINT', '#MAILCAMP_ENDPOINT#');

class ApiMethodsTest extends PHPUnit_Framework_TestCase {

    public function testCheckTokenXml() {
        $api = new Mailcamp_Api(USERNAME, USERTOKEN, ENDPOINT);
        $response = $api->checkToken();

        $this->assertEquals('SUCCESS', $response->toArray['status']);
    }

    public function testAddSubscriberToList() {
        $api = new Mailcamp_Api(USERNAME, USERTOKEN, ENDPOINT);
        $response = $api->addSubscriberToList(array(
            'mailinglist'   =>  11,
            'emailaddress'  =>  'slawomir.zytko@gmail.com',
            'format'    =>  'html',
            'confirmed' =>  1,
            'customfields' => array(
                array(
                    'fieldid'    =>  '8',
                    'value'     =>  'Cracov'
                ),
                array(
                    'fieldid'   =>  '2',
                    'value'     =>  'Slawek'
                ),
                array(
                    'fieldid'   =>  '3',
                    'value'     =>  'Zytko'
                )
            )
        ));


        if ($response->isSuccess == false) {
            $this->assertStringMatchesFormat('%d', $response->toArray['errormessage']);
        } else {
            $this->assertStringMatchesFormat('%d', $response->toArray['data']);
        }
    }

    public function testIsSubscriberOnList() {
        $api = new Mailcamp_Api(USERNAME, USERTOKEN, ENDPOINT);
        $response = $api->isSubscriberOnList(array(
            'mailinglist' => 11,
            'emailaddress' => 'slawomir.zytko@gmail.com'
        ));

        $this->assertStringMatchesFormat('%d', $response->toArray['data']);
    }

    public function testGetSubscribers() {
        $api = new Mailcamp_Api(USERNAME, USERTOKEN, ENDPOINT);
        $response = $api->getSubscribers(array(
            'mailinglist' => 11
        ));

        $this->assertInternalType('array', $response->toArray['data']['subscriberlist']);
    }

    public function testGetLists() {
        $api = new Mailcamp_Api(USERNAME, USERTOKEN, ENDPOINT);
        $response = $api->getLists();

        $this->assertInternalType('array', $response->toArray['data']);
    }

    public function testGetCustomFields() {
        $api = new Mailcamp_Api(USERNAME, USERTOKEN, ENDPOINT);
        $response = $api->getCustomFields(array(
            'mailinglist' => 11
        ));

        $this->assertInternalType('array', $response->toArray['data']);
    }

    public function testEditSubscriberCustomFields() {
        $api = new Mailcamp_Api(USERNAME, USERTOKEN, ENDPOINT);

        $response = $api->editSubscriberCustomFields(array(
            'mailinglist'   =>  11,
            'emailaddress'  =>  'slawomir.zytko@gmail.com',
            'customfields' => array(
                array(
                    'fieldid'    =>  '8',
                    'value'     =>  'Amsterdam'
                ),
                array(
                    'fieldid'   =>  '2',
                    'value'     =>  'Sławomir'
                ),
                array(
                    'fieldid'   =>  '3',
                    'value'     =>  'Żytko'
                )
            )
        ));

        $this->assertStringMatchesFormat('%d', $response->toArray['data']);
    }

    public function testDeleteSubscriber() {
        $api = new Mailcamp_Api(USERNAME, USERTOKEN, ENDPOINT);
        $response = $api->deleteSubscriber(array(
            'mailinglist'  =>  11,
            'emailaddress'  =>  'slawomir.zytko@gmail.com'
        ));

        $this->assertEquals('SUCCESS', $response->toArray['status']);
    }
}
