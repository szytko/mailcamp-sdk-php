<?php
/**
 *
 * @author    Sławomir Żytko <slawomir.zytko@gmail.com>
 * @copyright (c) 2013, PitGroup (http://pitgroup.nl)
 */

if (!defined('USE_AUTOLOADER')) define('USE_AUTOLOADER', false);
require_once '../Mailcamp/autoload.php';

class XmlTest extends PHPUnit_Framework_TestCase {

    public function testUsernameUsertokenNodes() {
        $username = 'username';
        $usertoken = 'usertoken';

        $api = new Mailcamp_Api($username, $usertoken, 'http://mailcamp.eu/development/xml.php');
        $actualXml = $api->initXmlBuilder();

        $expectedXml = simplexml_load_string('<xmlrequest><username>username</username><usertoken>usertoken</usertoken></xmlrequest>');

        $this->assertXmlStringEqualsXmlString($expectedXml->asXML(), $actualXml->build());
    }

}
