<?php
/**
 *
 * @author    Sławomir Żytko <slawomir.zytko@gmail.com>
 * @copyright (c) 2013, PitGroup (http://pitgroup.nl)
 */

if (!defined('USE_AUTOLOADER')) define('USE_AUTOLOADER', false);
require_once '../Mailcamp/autoload.php';

class ApiClientTest extends PHPUnit_Framework_TestCase {

    public function testClientInstanceOf() {
        $api = new Mailcamp_Api('username', 'token', 'endpoint');
        $this->assertInstanceOf('Mailcamp_Http_Adapter_ClientInterface', $api->getClient());
    }

}
