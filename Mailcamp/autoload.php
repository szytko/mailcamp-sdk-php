<?php
/**
 * Autoloader
 *
 * @copyright (c) 2013, MailCamp (http://mailcamp.eu)
 * @author    Sławomir Żytko <slawomir.zytko@gmail.com>
 * @version 1.0.0a
 */

if (defined('USE_AUTOLOADER') && USE_AUTOLOADER == true) {
    /**
     * Defines __autoload function
     * @param $className
     */
    function __autoload($className) {
        $extension = ".php";
        $path = realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR;
        $className = str_replace("_" , DIRECTORY_SEPARATOR, $className);
        $filename = $path . $className . $extension;
        if (is_readable($filename)) {
            require_once $filename;
        }
    }
} else {
    set_include_path(
        get_include_path() . ':' .
        realpath(dirname(__FILE__) . '/..')
    );
    require_once 'Mailcamp/Api.php';
    require_once 'Mailcamp/ApiException.php';
    require_once 'Mailcamp/XmlBuilder.php';
    require_once 'Mailcamp/Http/Client.php';
    require_once 'Mailcamp/Http/Adapter/ClientInterface.php';
    require_once 'Mailcamp/Http/HttpAdapterException.php';
    require_once 'Mailcamp/Http/Response.php';
}
?>