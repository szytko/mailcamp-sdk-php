<?php
/**
 * Class parses response from XML API
 *
 * @copyright (c) 2013, MailCamp (http://mailcamp.eu)
 * @author    Sławomir Żytko <slawomir.zytko@gmail.com>
 * @version 1.0.0a
 */

class Mailcamp_Http_Response {

    /**
     * Extracts the response code from a response string
     *
     * @param string $responseStr
     * @return int
     */
    protected static function extractCode($responseStr) {
        preg_match("|^HTTP/[\d\.x]+ (\d+)|", $responseStr, $code);

        if (!isset($code[1]))
            return false;

        return $code[1];
    }

    /**
     * Extracts the HTTP message from a response string
     *
     * @param string $responseStr
     * @return string
     */
    protected static function extractMessage($responseStr) {
        preg_match("|^HTTP/[\d\.x]+ \d+ ([^\r\n]+)|", $responseStr, $msg);

        if (!isset($msg[1]))
            return false;

        return $msg[1];
    }

    /**
     * Extracts the HTTP version from a response string
     *
     * @param string $responseStr
     * @return string
     */
    protected static function extractVersion($responseStr) {
        preg_match("|^HTTP/([\d\.x]+) \d+|", $responseStr, $ver);

        if (!isset($ver[1]))
            return false;

        return $ver[1];
    }

    /**
     * Extracts the headers from a response string
     *
     * @param   string $responseStr
     * @return  array
     */
    protected static function extractHeaders($responseStr) {
        $headers = array();

        $parts = preg_split('|(?:\r?\n){2}|m', $responseStr, 2);
        if (!$parts[0])
            return $headers;

        $lines = explode("\n", $parts[0]);
        unset($parts);
        $last_header = null;

        foreach ($lines as $line) {
            $line = trim($line, "\r\n");
            if ($line == "")
                break;

            if (preg_match("|^([\w-]+):\s*(.+)|", $line, $hdrs)) {
                unset($last_header);
                $h_name = strtolower($hdrs[1]);
                $h_value = $hdrs[2];

                if (isset($headers[$h_name])) {
                    if (!is_array($headers[$h_name])) {
                        $headers[$h_name] = array($headers[$h_name]);
                    }

                    $headers[$h_name][] = $h_value;
                } else {
                    $headers[$h_name] = $h_value;
                }
                $last_header = $h_name;
            } elseif (preg_match("|^\s+(.+)$|", $line, $hdrs) && $last_header !== null) {
                if (is_array($headers[$last_header])) {
                    end($headers[$last_header]);
                    $last_header_key = key($headers[$last_header]);
                    $headers[$last_header][$last_header_key] .= $hdrs[1];
                } else {
                    $headers[$last_header] .= $hdrs[1];
                }
            }
        }

        return $headers;
    }

    /**
     * Extracts the body from a response string
     *
     * @param string $responseStr
     * @return string
     */
    protected static function extractBody($responseStr) {
        $bodyParts = preg_split('|(?:\r?\n){2}|m', $responseStr, 2);
        if (!isset($bodyParts[1]))
            return '';

        return $bodyParts[1];
    }

    /**
     * Parses response and extracts information
     *
     * Returns object:
     * {
     *      status          Status of HTTP Request
     *      headers         Headers returned by server
     *      body            Body returned by server
     *      version         Version of HTTP protocol
     *      message         Message returned by server
     *      toArray         Body is originally returned in XML format. toArray property contains body converted to array
     *      isOk            Determines if HTTP Request was successfully
     *      isSuccess       Determines if operation was successfully
     * }
     *
     *
     * @param string $responseStr
     * @return stdClass
     */
    public static function extract($responseStr) {
        $objResponse = new stdClass();

        $objResponse->status = self::extractCode($responseStr);
        $objResponse->headers = self::extractHeaders($responseStr);

        $objResponse->body = self::extractBody($responseStr);
        $objResponse->version = self::extractVersion($responseStr);
        $objResponse->message = self::extractMessage($responseStr);

        //xml to array conversion
        $objResponse->toArray = json_decode(json_encode((array)simplexml_load_string($objResponse->body)),1);

        $objResponse->isOk = $objResponse->status == 200;
        $objResponse->isSuccess = $objResponse->toArray['status'] == 'SUCCESS';

        return $objResponse;

    }
}
