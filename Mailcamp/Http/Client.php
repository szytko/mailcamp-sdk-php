<?php
/**
 * Class uses to get instance of http client adapter by name
 *
 * @copyright (c) 2013, MailCamp (http://mailcamp.eu)
 * @author    Sławomir Żytko <slawomir.zytko@gmail.com>
 * @version 1.0.0a
 */

final class Mailcamp_Http_Client {

    /**
     * POST method of request
     */
    const METHOD_POST = 'POST';

    /**
     * GET method of request
     */
    const METHOD_GET = 'GET';

    /**
     * Get http client adapter
     *
     * @param string $name
     * @return object
     * @throws Mailcamp_ApiException
     */
    public static function getAdapter($name = 'Curl') {
        $adapterClassNamePrefix = 'Mailcamp_Http_Adapter_';

        $name = ucfirst($name);

        if (!defined('USE_AUTOLOADER') || USE_AUTOLOADER == false) {
            require_once 'Mailcamp/Http/Adapter/' . $name . '.php';
        }
        if (!class_exists($adapterClassNamePrefix . $name)) {
            throw new Mailcamp_ApiException(sprintf("Http Adapter %s not found", $name));
        }

        $reflectionClass = new ReflectionClass($adapterClassNamePrefix . $name);
        return $reflectionClass->newInstance();
    }

    /**
     * Get default client http adapter
     *
     * @return object
     */
    public static function getDefaultAdapter() {

        return self::getAdapter('Curl');
    }
}
