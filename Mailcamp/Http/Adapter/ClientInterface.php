<?php
/**
 * Http Client interface
 *
 * @copyright (c) 2013, MailCamp (http://mailcamp.eu)
 * @author    Sławomir Żytko <slawomir.zytko@gmail.com>
 * @version 1.0.0a
 */

interface Mailcamp_Http_Adapter_ClientInterface {

    /**
     * Set client options
     *
     * @param array $options
     * @return mixed
     */
    public function setOptions($options = array());

    /**
     * Get all client options or one indicated option
     *
     * @param bool $optionName
     * @return mixed
     */
    public function getOptions($optionName = false);

    /**
     * Set indicated client option
     *
     * @param $optionName
     * @param $optionValue
     * @return mixed
     */
    public function setOption($optionName, $optionValue);

    /**
     * Send request to indicated URI
     *
     * @param $uri
     * @return mixed
     */
    public function sendRequest($uri);

    /**
     * Get response from server
     *
     * @return mixed
     */
    public function getResponse();

    /**
     * Close connection
     *
     * @return mixed
     */
    public function closeConnection();

    /**
     * Set method type
     *
     * @param $method
     * @return mixed
     */
    public function setMethod($method);

    /**
     * Set request headers
     *
     * @param array $headers
     * @return mixed
     */
    public function setHeaders($headers = array());

    /**
     * Set request body
     *
     * @param null $body
     * @return mixed
     */
    public function setBody($body = null);

}
