<?php
/**
 * Http Client based on cURL extension
 *
 * @copyright (c) 2013, MailCamp (http://mailcamp.eu)
 * @author    Sławomir Żytko <slawomir.zytko@gmail.com>
 * @version 1.0.0a
 */

class Mailcamp_Http_Adapter_Curl implements Mailcamp_Http_Adapter_ClientInterface {

    /**
     * cURL options
     *
     * @var array
     */
    private $options = array(
        'port'  =>  80
    );

    /**
     * Headers to send in request
     *
     * @var array
     */
    private $headers = array();

    /**
     * Request body
     *
     * @var array
     */
    private $body = array();

    /**
     * Request default method
     *
     * @var string
     */
    private $method = Mailcamp_Http_Client::METHOD_POST;

    /**
     * cURL handle
     *
     * @var null
     */
    private $connection = null;

    /**
     * Response body
     *
     * @var null
     */
    private $responseStr = null;

    /**
     * Checks if cURL extension is loaded
     */
    public function __construct() {
        if (!extension_loaded('curl'))
            throw new Mailcamp_Http_HttpAdapterException('cURL extension has not beed loaded.');
    }

    /**
     * Sets cURL options
     *
     * @param array $options
     * @return Mailcamp_Http_Adapter_Curl
     */
    public function setOptions($options = array()) {
        $this->options = $options;

        return $this;
    }

    /**
     * Sets cURL option.
     *
     * Provide third parameter as true to overwrite existing option with another value
     *
     * @param string $optionName        Name of option to set
     * @param mixed $optionValue        Option value
     * @param bool $overwrite           Overwrite existing option
     * @return Mailcamp_Http_Adapter_Curl
     * @throws Mailcamp_Http_HttpAdapterException
     */
    public function setOption($optionName, $optionValue, $overwrite = false) {
        //check if option name is a scalar value
        if (!is_scalar($optionName))
            throw new Mailcamp_Http_HttpAdapterException('Option name must be a scalar value');


        //check if option is overwritten
        if (isset($this->options[$optionName]) && !$overwrite)
            throw new Mailcamp_Http_HttpAdapterException(sprintf('Option "%s" is already set. Set third parameter as TRUE to overwrite existing option', $optionName));

        //set new option
        $this->options[$optionName] = $optionValue;

        return $this;
    }

    /**
     * Get cURL options.
     *
     * Provide option name to get a specific cURL option
     *
     * @param string $optionName    Name of specific option
     * @return array
     * @throws Mailcamp_Http_HttpAdapterException
     */
    public function getOptions($optionName = false) {
        //get all options
        if (false == $optionName) {
            return $this->options;
        }

        //check if name of option is a scalar value
        if (!is_scalar($optionName))
            throw new Mailcamp_Http_HttpAdapterException('Option name must be a scalar value');

        //check if indicated options has been already set
        if (!isset($this->options[$optionName]))
            return false;


        //return indicated option
        return $this->options[$optionName];
    }

    /**
     * Initializes cURL connection on specified port
     *
     * @param $host         Host name
     * @param int $port     Port to connect on
     * @param bool $secure  Connect using Secure Socket Layer
     * @return resource
     * @throws Mailcamp_Http_HttpAdapterException
     */
    protected function initialize($host, $port = 80, $secure = false) {
        //close current connection if exists
        if ($this->connection) {
            $this->closeConnection();
        }

        //initialize cURL
        $this->connection = curl_init();

        //verify port
        if (!is_numeric($port) || $port < 0 || $port > 65535)
            throw new Mailcamp_Http_HttpAdapterException('Invalid port');

        //cURL is using port 80 as default
        if ($port != 80) {
            curl_setopt($this->connection, CURLOPT_PORT, $port);
        }

        //set cURL timeout
        if (($timeout = $this->getOptions('timeout'))) {
            curl_setopt($this->connection, CURLOPT_TIMEOUT, $timeout);
        }

        //set cURL max redirects
        if (($maxredirects = $this->getOptions('maxredirects'))) {
            curl_setopt($this->connection, CURLOPT_MAXREDIRS, $maxredirects);
        }

        //setup secure connection
        if (false !== $secure) {
            //set SSL certificate
            if (($sslCert = $this->getOptions('ssl_certificate'))) {
                curl_setopt($this->connection, CURLOPT_SSLCERT, $sslCert);
            }

            //set SSL certificate password phrase
            if (($sslPassPhrase = $this->getOptions('ssl_passphrase'))) {
                curl_setopt($this->connection, CURLOPT_SSLCERTPASSWD, $sslPassPhrase);
            }
        }

        //check cURL connection
        if (!$this->connection) {
            $this->closeConnection();

            throw new Mailcamp_Http_HttpAdapterException(sprintf('Unable to connect to %s:%s', $host, $port));
        }

        return $this->connection;
    }

    /**
     * Sends a request to remote host using cURL extension
     *
     * @param $uri          Uri to endpoint
     * @return stdClass
     * @throws Mailcamp_Http_HttpAdapterException
     */
    public function sendRequest($uri) {
        //initialize curl
        $this->initialize($uri, $this->getOptions('port'), $this->getOptions('secure'));
        //check uri
        if ($uri == '')
            throw new Mailcamp_Http_HttpAdapterException('Invalid URI');

        //set cURL uri
        curl_setopt($this->connection, CURLOPT_URL, $uri);

        //set http version to use
        if (($httpVer = $this->getOptions('http_ver'))) {
            switch ($httpVer) {
                case 1.0:
                    curl_setopt($this->connection, CURL_HTTP_VERSION_1_0, true);
                    break;
                default:
                    curl_setopt($this->connection, CURL_HTTP_VERSION_1_1, true);
                    break;
            }
        }

        //set request method
        switch ($this->method) {
            case Mailcamp_Http_Client::METHOD_GET:
                curl_setopt($this->connection, CURLOPT_HTTPGET, true);
                break;
            case Mailcamp_Http_Client::METHOD_POST:
                curl_setopt($this->connection, CURLOPT_POST, true);
                break;
        }

        //set cURL header and response options
        curl_setopt($this->connection, CURLOPT_HEADER, true);
        curl_setopt($this->connection, CURLOPT_RETURNTRANSFER, true);

        //set additional request headers
        if (is_array($this->headers) && !empty($this->headers)) {
            $this->headers['Accept'] = '';
            curl_setopt($this->connection, CURLOPT_HTTPHEADER, $this->headers);
        }

        //set request body in case of POST request
        if ($this->method == Mailcamp_Http_Client::METHOD_POST) {
            curl_setopt($this->connection, CURLOPT_POSTFIELDS, $this->body);
        }

        //send the request
        $this->responseStr = curl_exec($this->connection);

        return $this->getResponse();
    }

    /**
     * Parses response string from request
     *
     * @return stdClass
     * @throws Mailcamp_Http_HttpAdapterException
     */
    public function getResponse() {
        if (null == $this->responseStr)
            throw new Mailcamp_Http_HttpAdapterException('Response is empty');

        return Mailcamp_Http_Response::extract($this->responseStr);
    }

    /**
     * Closes cURL connection
     */
    public function closeConnection() {
        if (is_resource($this->connection)) {
            curl_close($this->connection);
        }
    }

    /**
     * Sets request method
     *
     * @param $method
     * @return Mailcamp_Http_Adapter_Curl
     */
    public function setMethod($method) {
        $this->method = $method;

        return $this;
    }

    /**
     * Sets additional request headers
     *
     * @param array $headers
     * @return Mailcamp_Http_Adapter_Curl
     */
    public function setHeaders($headers = array()) {
        $this->headers = $headers;

        return $this;
    }

    /**
     * Sets request body
     *
     * @param null $body
     * @return Mailcamp_Http_Adapter_Curl
     */
    public function setBody($body = null) {
        $this->body = $body;

        return $this;
    }
}
