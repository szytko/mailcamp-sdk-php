<?php
/**
 * Class handles Http Adapter exceptions
 *
 * @copyright (c) 2013, MailCamp (http://mailcamp.eu)
 * @author    Sławomir Żytko <slawomir.zytko@gmail.com>
 * @version 1.0.0a
 */

class Mailcamp_Http_HttpAdapterException extends Mailcamp_ApiException {

}
