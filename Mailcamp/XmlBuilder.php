<?php
/**
 * Class builds XML from associative array
 *
 * @copyright (c) 2013, MailCamp (http://mailcamp.eu)
 * @author    Sławomir Żytko <slawomir.zytko@gmail.com>
 * @version 1.0.0a
 */

class Mailcamp_XmlBuilder {

    /**
     * Pointer to parent node
     *
     * @var null
     */
    private $parent;

    /**
     * XML tree root name
     *
     * @var
     */
    private $xmlRootName;

    /**
     * Current node pointer
     *
     * @var array
     */
    private $currentNode;

    /**
     * Node constructor
     *
     * @param $xmlRootName
     * @param null $parent
     */
    public function __construct($xmlRootName, $parent = null, $node = null) {
        $this->parent = $parent;
        $this->xmlRootName = $xmlRootName;
        if ($node == null)
            $this->currentNode = simplexml_load_string(sprintf('<%s />', $this->xmlRootName));
        else {
            $this->currentNode = $node->addChild($xmlRootName);
        }
    }

    /**
     * Add new xml node
     *
     * @param $nodeName
     * @return Mailcamp_XmlBuilder
     */
    public function newNode($nodeName) {
        return new Mailcamp_XmlBuilder($nodeName, $this, $this->currentNode);
    }

    /**
     * Closes current node
     *
     * @return null
     */
    public function closeNode() {
        return $this->parent;
    }

    /**
     * Adds new value in node
     *
     * @param $name
     * @param $val
     * @return Mailcamp_XmlBuilder
     */
    public function addValue($name, $val = null) {
        $this->currentNode->addChild($name, $val);

        return $this;
    }

    /**
     * Adds new value in node if not null
     *
     * @param $name
     * @param $val
     * @return $this
     */
    public function addValueIfNotNull($name, $val) {
        if (null != $val) {
            $this->currentNode->addChild($name, $val);
        }

        return $this;
    }

    /**
     * Adds values from array in node
     *
     * @param $name
     * @param $array
     * @return Mailcamp_XmlBuilder
     */
    public function addArray($name, $array) {
        $node = $this->currentNode->addChild($name);
        foreach ($array as $key => $value) {
            $node->addChild($key, $value);
        }

        return $this;
    }

    /**
     * Adds values from multi dimension array in node
     *
     * @param $name
     * @param $array
     * @return Mailcamp_XmlBuilder
     */
    public function addMultiArray($name, $array) {
        foreach ($array as $key => $arrayValues) {
            $this->addArray($name, $arrayValues);
        }

        return $this;
    }

    /**
     * Builds XML string from array
     *
     * @return bool
     */
    public function build() {
        return $this->currentNode->asXML();
    }
}
