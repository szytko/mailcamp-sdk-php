<?php
/**
 * Wrapper to interact with Mailcamp XML Api
 *
 * @copyright (c) 2013, MailCamp (http://mailcamp.eu)
 * @author    Sławomir Żytko <slawomir.zytko@gmail.com>
 * @version 1.0.0a
 */

/**
 *
 * Available api methods:
 *      checkToken                      Validates user token
 *      addSubscriberToList             Adds a subscriber and associated custom details
 *      getLists                        Retrieves a list of all the Contact Lists that are located in Mailcamp
 *      deleteSubscriber                Deletes a contact from specified contact list.
 *      getSubscribers                  Retrieves a count and list of contacts from the indicated contact list
 *      getCustomFields                 Retrieves a contact lists custom field data
 *      isSubscriberOnList              Checks if email address is located in indicated contact List
 *      editSubscriberCustomFields      Edits custom details associated with existing subscriber
 */

class Mailcamp_Api {
    /**
     * Mailcamp SDK version
     */
    const VERSION = '1.0.0a';

    /**
     * @var The user name used to login to the Mailcamp
     */
    private $username;

    /**
     * @var The unique token assigned to the user account
     */
    private $usertoken;

    /**
     * @var The Mailcamp API endpoint
     */
    private $endpoint;

    /**
     * Instance of XML builder
     *
     * @var Mailcamp_XmlBuilder
     */
    private $xmlBuilder;

    /**
     * @var object  Default Http Client
     */
    private $client;

    /**
     * Set required api parameters
     *
     * @param $username     The user name used to login to the Mailcamp. (Required)
     * @param $usertoken    The unique token assigned to the user account. (Required)
     * @param $endpoint     The url to Mailcamp API. (Required)
     * @throws InvalidUsernameException
     * @throws InvalidUsertokenException
     * @throws InvalidEndpointException
     */
    public function __construct($username, $usertoken, $endpoint) {
        //check username, usertoken and api endpoint
        if (!$username) throw new InvalidUsernameException();
        if (!$usertoken) throw new InvalidUsertokenException();
        if (!$endpoint) throw new InvalidEndpointException();
        $this->username = $username;
        $this->usertoken = $usertoken;
        $this->endpoint = $endpoint;

        //get default http adapter
        $this->client = Mailcamp_Http_Client::getDefaultAdapter();
    }

    /**
     * Initializes XML Builder
     *
     * Sets XML root node
     * Adds username and usertoken nodes
     *
     * @return Mailcamp_XmlBuilder
     */
    public function initXmlBuilder() {
        //init xml builder
        //set root node
        $this->xmlBuilder = new Mailcamp_XmlBuilder('xmlrequest');

        //add required values to xml
        $this->xmlBuilder
            ->addValue('username', $this->username)
            ->addValue('usertoken', $this->usertoken);

        return $this->xmlBuilder;
    }

    /**
     * Sets http client options
     *
     * @param array $options
     * @return $this
     */
    public function setClientOptions($options = array()) {
        $this->client->setOptions($options);

        return $this;
    }

    /**
     * Get http client instance
     *
     * @return Mailcamp_Http_Adapter_ClientInterface
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * Sets http client adapter
     * Http Client must implements Mailcamp_Http_Adapter_ClientInterface
     *
     * @param Mailcamp_Http_Adapter_ClientInterface $client
     * @return $this
     */
    public function setClient(Mailcamp_Http_Adapter_ClientInterface $client) {
        $this->client = $client;

        return $this;
    }

    /**
     * Validates data required for invoked method
     * Throws exception if required data is not provided
     *
     * @param array $required
     * @param array $data
     * @return bool
     * @throws MissingRequiredDataException
     */
    private function validateData(array $required, array $data) {
        foreach ($required as $field) {
            if (isset($data[$field]) && null != $data[$field]) continue;

            throw new MissingRequiredDataException(sprintf("%s is required", $field));
        }

        return true;
    }

    /**
     * Sends a request
     *
     * @param $xmlString
     * @return mixed
     */
    private function sendRequest($xmlString) {
        //set body and method type
        $this->client->setBody($xmlString);
        $this->client->setMethod(Mailcamp_Http_Client::METHOD_POST);

        //set request to specified endpoint
        $response = $this->client->sendRequest($this->endpoint);

        //close connection after request
        $this->client->closeConnection();

        return $response;
    }

    /**
     * Validates user token
     *
     * No parameters are required in that method
     *
     * Response:
     *      Method returns response as an object of stdClass. Response body is in XML format and can be accessible
     *      as an array in `toArray` property. XML API returns true in case of successful authorization.
     *
     *      Status of request can be check as follows:
     *      $response->toArray['status']
     *              SUCCESS     for a successful response
     *              ERROR       for a failed request
     *
     *      In case of failed operation, an error message can be accessible as follows:
     *      $response->toArray['errormessage']
     *
     * @return stdClass         Parsed response
     */
    public function checkToken() {
        $this->initXmlBuilder();
        $this->xmlBuilder
                ->addValue('requesttype', 'authentication')
                ->addValue('requestmethod', 'xmlapitest')
                ->addValue('details', PHP_EOL);

        //convert xml to string
        $xmlString = $this->xmlBuilder->build();

        return $this->sendRequest($xmlString);
    }

    /**
     * Adds a subscriber and associated custom details
     *
     * Required API parameters:
     *      emailaddress        The email address of the new contact
     *      mailinglist         The list that the contact is located within
     *
     * Optional API parameters:
     *      confirmed           Sets the confirmation status of the subscriber to confirmed or not
     *                          (yes or y or true or 1) (default to unconfirmed)
     *      format              The format of the email campaigns that this contact prefers to receive
     *                          (html or h or text or t) (defaults to text)
     *      customfields:
     *          fieldid         The id of the custom field to add
     *          value           The value of custom field
     *
     * Response:
     *      Method returns response as an object of stdClass. Response body is in XML format and can be accessible
     *      as an array in `toArray` property. XML API returns new contact id number.
     *
     *      New subscriber id can be accessible as follows:
     *      $response->toArray['data']
     *
     *      Status of request can be check as follows:
     *      $response->toArray['status']
     *              SUCCESS     for a successful response
     *              ERROR       for a failed request
     *
     *      In case of failed operation, an error message can be accessible as follows:
     *      $response->toArray['errormessage']
     *
     * @param array $data       Required data in associative array
     *                          Required keys: emailaddress, mailinglist
     * @return stdClass         Parsed response
     */
    public function addSubscriberToList(array $data) {
        $this->validateData(array(
            'emailaddress', 'mailinglist'
        ), $data);

        if (!isset($data['customfields'])) $data['customfields'] = array();
        if (!isset($data['format'])) $data['format'] = 'html';
        if (!isset($data['confirmed'])) $data['confirmed'] = 0;

        $this->initXmlBuilder();
        $this->xmlBuilder
            ->addValue('requesttype', 'subscribers')
            ->addValue('requestmethod', 'AddSubscriberToList')
            ->newNode('details')
                ->addValue('emailaddress',  $data['emailaddress'])
                ->addValue('mailinglist', $data['mailinglist'])
                ->addValueIfNotNull('format', $data['format'])
                ->addValueIfNotNull('confirmed', $data['confirmed'])
                ->newNode('customfields')
                    ->addMultiArray('item', $data['customfields'])
                ->closeNode()
            ->closeNode();

        //convert xml to string
        $xmlString = $this->xmlBuilder->build();

        return $this->sendRequest($xmlString);
    }

    /**
     * Retrieves a list of all the Contact Lists that are located in Mailcamp
     *
     * No parameters are required in that method
     *
     * Response:
     *      Method returns response as an object of stdClass. Response body is in XML format and can be accessible
     *      as an array in `toArray` property. XML API returns list of contact lists.
     *
     *      Array of contact lists can be accessible as follows:
     *      $response->toArray['data']
     *
     *      Description of returned list
     *
     *          array(
     *              item    =>  array(
     *                  listid                  =>  #   The id of the list.
     *                  name                    =>  #   The name of the list.
     *                  subscribecount          =>  #   A count of how many subscribed contacts.
     *                  unsubscribecount        =>  #   A count of how many unsubscribed contacts.
     *                  autorespondercount      =>  #   A count of how many autoresponders are linked to the Contact List.
     *              )
     *          ),
     *          ...
     *
     *
     *      Status of request can be check as follows:
     *      $response->toArray['status']
     *              SUCCESS     for a successful response
     *              ERROR       for a failed request
     *
     *      In case of failed operation, an error message can be accessible as follows:
     *      $response->toArray['errormessage']
     *
     * @return stdClass         Parsed response
     */
    public function getLists() {
        $this->initXmlBuilder();
        $this->xmlBuilder
            ->addValue('requesttype', 'user')
            ->addValue('requestmethod', 'GetLists')
            ->addValue('details', PHP_EOL);

        //convert xml to string
        $xmlString = $this->xmlBuilder->build();

        return $this->sendRequest($xmlString);
    }

    /**
     * Deletes a contact from specified contact list.
     *
     * Required API parameters:
     *      emailaddress        The email address of contact which will be deleted
     *      mailinglist         The ID of the Contact List that contains indicated email address
     *
     * Response:
     *      Method returns response as an object of stdClass. Response body is in XML format and can be accessible
     *      as an array in `toArray` property. XML API returns true when contact is deleted correctly
     *
     *      Status of request can be check as follows:
     *      $response->toArray['status']
     *              SUCCESS     for a successful response
     *              ERROR       for a failed request
     *
     *      In case of failed operation, an error message can be accessible as follows:
     *      $response->toArray['errormessage']
     *
     * @param array $data       Required data in associative array
     *                          Required keys:  mailinglist, emailaddress
     * @return stdClass         Parsed response
     */
    public function deleteSubscriber(array $data) {
        $this->validateData(array(
            'mailinglist', 'emailaddress'
        ), $data);

        $this->initXmlBuilder();
        $this->xmlBuilder
            ->addValue('requesttype', 'subscribers')
            ->addValue('requestmethod', 'DeleteSubscriber')
            ->newNode('details')
                ->addValue('list', $data['mailinglist'])
                ->addValue('emailaddress',  $data['emailaddress'])
            ->closeNode();

        //convert xml to string
        $xmlString = $this->xmlBuilder->build();

        return $this->sendRequest($xmlString);
    }

    /**
     * Retrieves a contact lists custom field data
     *
     * Required API parameters:
     *      mailinglist             The ID’s of the contact list that the custom field data is being requested
     *
     * Response:
     *      Method returns response as an object of stdClass. Response body is in XML format and can be accessible
     *      as an array in `toArray` property. XML API returns list of custom fields associated with indicated
     *      contact list.
     *
     *      Array of custom fields can be accessible as follows:
     *      $response->toArray['data']
     *
     *      Description of returned list
     *
     *          array(
     *              item    =>  array(
     *                  fieldid                 =>  #   The custom fields ID number.
     *                  name                    =>  #   The name of the custom field.
     *                  fieldtype               =>  #   the type of field (text, number etc.).
     *                  defaultvalue            =>  #   If you set a default value it will appear here.
     *                  required                =>  #   If this field is required to be filled in (1 or 0).
     *                  fieldsettings           =>  #   Serialized version of the custom fields settings
     *              )
     *          ),
     *          ...
     *
     *
     *      Status of request can be check as follows:
     *      $response->toArray['status']
     *              SUCCESS     for a successful response
     *              ERROR       for a failed request
     *
     *      In case of failed operation, an error message can be accessible as follows:
     *      $response->toArray['errormessage']
     *
     * @param array $data       Required data in associative array
     *                          Required keys:  mailinglist
     * @return stdClass         Parsed response
     */
    public function getCustomFields(array $data) {
        $this->validateData(array(
            'mailinglist'
        ), $data);

        $this->initXmlBuilder();
        $this->xmlBuilder
            ->addValue('requesttype', 'lists')
            ->addValue('requestmethod', 'GetCustomFields')
            ->newNode('details')
                ->addValue('listids', $data['mailinglist'])
            ->closeNode();

        //convert xml to string
        $xmlString = $this->xmlBuilder->build();

        return $this->sendRequest($xmlString);
    }

    /**
     * Retrieves a count and list of contacts from the indicated contact list
     *
     * Required API parameters:
     *      mailinglist                The ID of the contact list
     *      emailaddress               The email address (or domain) of the contact
     *
     * Response:
     *      Method returns response as an object of stdClass. Response body is in XML format and can be accessible
     *      as an array in `toArray` property. XML API returns count and list of subscribers from the indicated
     *      contact list.
     *
     *      Count of subscribers can be accessible as follows:
     *      $response->toArray['data']['count']
     *
     *      Array of subscribers can be accessible as follows:
     *      $response->toArray['data']['subscriberlist']
     *
     *
     *      Status of request can be check as follows:
     *      $response->toArray['status']
     *              SUCCESS     for a successful response
     *              ERROR       for a failed request
     *
     *      In case of failed operation, an error message can be accessible as follows:
     *      $response->toArray['errormessage']
     *
     * @param array $data       Required data in associative array
     *                          Required keys:  mailinglist, emailaddress
     * @return  stdClass        Parsed response
     */
    public function getSubscribers(array $data) {
        $this->validateData(array(
            'mailinglist'
        ), $data);

        if (!isset($data['emailaddress'])) $data['emailaddress'] = null;

        $this->initXmlBuilder();
        $this->xmlBuilder
            ->addValue('requesttype', 'subscribers')
            ->addValue('requestmethod', 'GetSubscribers')
            ->newNode('details')
                ->newNode('searchinfo')
                    ->addValue('List', $data['mailinglist'])
                    ->addValueIfNotNull('Email', $data['emailaddress'])
                ->closeNode()
            ->closeNode();

        //convert xml to string
        $xmlString = $this->xmlBuilder->build();

        return $this->sendRequest($xmlString);
    }

    /**
     * Checks if email address is located in indicated contact List
     *
     * Required API parameters:
     *      emailaddress                The email address of the contact
     *      mailinglist                 The ID of the contact list where email address will be search
     *
     * Response:
     *      Method returns response as an object of stdClass. Response body is in XML format and can be accessible
     *      as an array in `toArray` property. XML API returns true if email address is located
     *
     *      The result of success operation can be accessible as follows:
     *      $response->toArray['data']
     *
     *      Status of request can be check as follows:
     *      $response->toArray['status']
     *              SUCCESS     for a successful response
     *              ERROR       for a failed request
     *
     *      In case of failed operation, an error message can be accessible as follows:
     *      $response->toArray['errormessage']
     *
     * @param array $data       Required data in associative array
     *                          Required keys:  mailinglist, emailaddress
     * @return  stdClass        Parsed response
     */
    public function isSubscriberOnList(array $data) {
        $this->validateData(array(
            'emailaddress', 'mailinglist'
        ), $data);

        $this->initXmlBuilder();
        $this->xmlBuilder
            ->addValue('requesttype', 'subscribers')
            ->addValue('requestmethod', 'IsSubscriberOnList')
            ->newNode('details')
                ->addValue('emailaddress',  $data['emailaddress'])
                ->addValue('listids', $data['mailinglist'])
            ->closeNode();

        //convert xml to string
        $xmlString = $this->xmlBuilder->build();

        return $this->sendRequest($xmlString);
    }

    /**
     * Edits custom details associated with existing subscriber
     *
     * Required API parameters:
     *      emailaddress        The email address of the editing contact
     *      mailinglist         The list that the contact is located within
     *
     * Optional API parameters:
     *      customfields:
     *          fieldid         The id of the editing custom field
     *          value           The value of custom field
     *
     * Response:
     *      Method returns response as an object of stdClass. Response body is in XML format and can be accessible
     *      as an array in `toArray` property. XML API returns ID number of edited contact
     *
     *      The ID number of edited contact can be accessible as follows:
     *      $response->toArray['data']
     *
     *      Status of request can be check as follows:
     *      $response->toArray['status']
     *              SUCCESS     for a successful response
     *              ERROR       for a failed request
     *
     *      In case of failed operation, an error message can be accessible as follows:
     *      $response->toArray['errormessage']
     *
     * @param array $data   Required data in associative array
     *                      Required keys: emailaddress, mailinglist
     * @return stdClass     Parsed response
     */
    public function editSubscriberCustomFields(array $data) {
        $this->validateData(array(
            'emailaddress', 'mailinglist'
        ), $data);

        if (!isset($data['customfields'])) $data['customfields'] = array();

        $this->initXmlBuilder();
        $this->xmlBuilder
            ->addValue('requesttype', 'subscribers')
            ->addValue('requestmethod', 'EditSubscriberCustomFields')
            ->newNode('details')
                ->addValue('emailaddress',  $data['emailaddress'])
                ->addValue('mailinglist', $data['mailinglist'])
                ->newNode('customfields')
                    ->addMultiArray('item', $data['customfields'])
                ->closeNode()
            ->closeNode();

        //convert xml to string
        $xmlString = $this->xmlBuilder->build();

        return $this->sendRequest($xmlString);
    }

}

/**
 * Api exceptions
 *
 * Class ApiException
 */
class ApiException extends Exception {}

/**
 * Api client exceptions
 *
 * Class ClientException
 */
class ClientException extends ApiException {}

/**
 * Invalid username exception
 *
 * Class InvalidUsernameException
 */
class InvalidUsernameException extends ApiException {}

/**
 * Invalid user token exception
 *
 * Class InvalidUsertokenException
 */
class InvalidUsertokenException extends ApiException {}

/**
 * Invalid Mailcamp endpoint exception
 *
 * Class InvalidEndpointException
 */
class InvalidEndpointException extends ApiException {}

/**
 * Missing required data exception
 *
 * Class MissingRequiredDataException
 */
class MissingRequiredDataException extends ApiException {}